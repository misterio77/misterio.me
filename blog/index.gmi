---
title: Latest Posts
description: My personal thoughts, rants, and occasional guide
---

{%- assign gmi_posts = site.posts | where: "gemini", true -%}

{%- for post in gmi_posts -%}
=> {{ post.url }} {{ post.title }}
{{ post.date | date: "%d/%m/%y" }}
{% if post.category.size > 0 %}({{ post.category }}){% endif %}
 {% for tag in post.tags %}#{{ tag }} {% endfor %}
{{ post.excerpt }}

{%- endfor -%}
