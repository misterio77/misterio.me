---
title: About me
---

Hey 👋, my name is Gabriel. I'm a brazillian programmer, designer, and activist.

I'm currently working on my Bachelor of Information Systems degree at [University of São Paulo](https://usp.br)'s [Institute of Mathematics and Computer Sciences](https://www.icmc.usp.br/en/), as a developer at [UGet](https://br.linkedin.com/company/u-get), and on a couple personal projects.

I'm interested in FLOSS, Distributed Systems, Software Engineering, and Functional Programming. Mainly working with Linux, Rust, Postgres, Nix, Python, Go, and C/C++.

## Tech and design

I've been into computers, science and math since i was little. I love Linux (FOSS in general), CLI programs, customization, and graphic design.

I enjoy modern and egornomical languagens, strong typing and high performance are nice to have.

I favor minimalism and composability in tools. I currently use [NixOS](https://nixos.org), [SwayWM](https://swaywm.org/), [Neovim](https://neovim.io), [Qutebrowser](https://qutebrowser.org), [NeoMutt](https://neomutt.org/) as daily drivers.

## Other stuff

I'm a huge fan of space and science fiction genres (games, books, artwork).

I love animals (i've got five cats and a dog), the planet, and the people who live in it.

## Causes

Studying at one of the big public universities of Brazil, meeting all sort of amazing people, and witnessing injustice has shaped how i think.

I'm an free software, environmental, queer, anti-capitalist, and education activist.

You'll probably see me avoiding proprietary software and walled gardens at all costs.
