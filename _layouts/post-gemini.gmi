---
layout: default-gemini
---
{{ page.date | date_to_string }} - {{ page.author }}

{{ content }}
