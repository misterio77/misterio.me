---
title: "Disciplinas BSI 020 - 2021/2"
category: bsi
language: pt-br
noindex: true
date: 2021-08-16
---

[Calculadora de notas](/notes/bsi/calculadora-2021-2.html)

## Horários
[Arquivo de calendário](https://misterio.me/files/2021/classes.ics)

- Segunda
  - 19:00 - 20:40: [Introdução à Pesquisa Operacional](#SME0510)
  - 21:00 - 22:40: [Sistemas Operacionais I](#SSC0541)
- Terça
  - 19:00 - 20:40: [Arquitetura de Computadores](#SSC0510)
  - 21:00 - 22:40: [Análise e Projeto Orientados à Objetos](#SSC0526)
- Quarta
  - 18:00 - 19:00: [Seminários em Computação II](#SSC0577)
  - 19:00 - 20:40: [Bases de Dados](#SCC0540)
  - 21:00 - 22:40: [Introdução à Pesquisa Operacional](#SME0510)
- Quinta
  - 19:00 - 20:40: [Sistemas Operacionais I](#SSC0541)
  - 21:00 - 22:40: [Contabilidade para Computação](#SEP0584)
- Sexta
  - 19:00 - 20:40: [Análise e Projeto Orientados à Objetos](#SSC0526)
  - 21:00 - 22:40: [Bases de Dados](#SCC0540)

--- 
## SME0510 - Introdução à Pesquisa Operacional
{: #SME0510 .ipo}

### Sobre
- Professora: Maristela Oliveira dos Santos ([mari@icmc.usp.br](mailto:mari@icmc.usp.br))
- Plataforma: [E-Disciplinas](https://edisciplinas.usp.br/course/view.php?id=91809)
- Aula: [Google Meet (Agendado, muda a cada aula)](https://meet.google.com/)

- Cobra presença? Não

### Avaliações
- Provas:
  - Prova 1: 22/09 - Modelagem e solução gráfica
  - Prova 2: 27/10 - Teoria básica e método simplex
  - Prova 3: 08/12 - Método de programação inteira e heurísticas
- Atividades:
  - Atividade semanal com correção automática

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SME0510)
- Notas das provas (NP): média aritmética entre as 3 provas
- Notas das atividades (NA): média aritmética de todas as atividades
- **Nota Final**: média ponderada entre NP (peso 80%) e NA (peso 20%)

--- 
## SSC0541 - Sistemas Operacionais I
{: #SSC0541 .so}

### Sobre
- Professor: Vanderlei Bonato ([vbonato@icmc.usp.br](mailto:vbonato@icmc.usp.br))
- Plataforma: [E-Disciplinas](https://edisciplinas.usp.br/course/view.php?id=92707)
- Aula: [Google Meet](https://edisciplinas.usp.br/mod/url/view.php?id=3778926)

- Cobra presença? [Sim, lista](https://edisciplinas.usp.br/mod/url/view.php?id=3778925)

### Avaliações
- Trabalhos práticos:
  - Trabalho 1: 17/10 - [Jantar dos filósofos](https://edisciplinas.usp.br/mod/assign/view.php?id=3778946)
- Provinhas:
  - Provinha 1: 26/08 - Gerenciamento de Processos
  - Provinha 2: 16/09 - Sincronização de Processos
  - Provinha 3: 14/10 - Gerenciamento de Memória
  - Provinha 4: 08/11 - Restante (armazenamento, I/O, e arquivos)
- Prova teórica única: 13/12

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SSC0541)
- Nota da prova teórica (PT)
- Nota das provinhas + trabalhos práticos (PV): média aritmética entre eles
- **Nota Final**: média ponderada entre PT (40%) e PV (60%)


--- 
## SSC0510 - Arquitetura de Computadores
{: #SSC0510}

### Sobre
- Professor: Eduardo Simões ([simoes@icmc.usp.br](mailto:simoes@icmc.usp.br))
- Plataforma: [Gitlab](https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0510-Arquitetura-de-Computadores)
- Aula: [Google Meet](https://meet.google.com/nwn-osys-cgu)

- Cobra presença? [Sim, lista](https://docs.google.com/spreadsheets/d/1xinFOyZHZA7YXygnpgPlY14TlVbwzIto6mpL_naHPNQ/edit?usp=sharing)

### Avaliações
- Prova (data?)
- Trabalho: em grupo, [planilha](https://docs.google.com/spreadsheets/d/18pFcYwvV9XkGrhZkdOhIoINsWKuKjthUsu5GGkf1xwM/edit?usp=sharing)

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SSC0510)
- Nota da prova (NP)
- Nota do trabalho (NT)
- **Nota Final**: média ponderada entre NP (70%) e NT (30%)

--- 
## SSC0526 - Análise e Projeto Orientados à Objetos
{: #SSC0526 .apoo}

### Sobre
- Professora: Rosana Braga ([rtvb@icmc.usp.br](mailto:rtvb@icmc.usp.br))
- PAE: Fabíola Malta ([fabiolamf@usp.br](mailto:fabiolamf@usp.br)), [grupo no whatsapp](https://chat.whatsapp.com/Dj7GkYUBPM04mcqbofK6HY)
- Plataforma: [E-Disciplinas](https://edisciplinas.usp.br/course/view.php?id=91587)
- Aula: [Google Meet](https://meet.google.com/fug-ccrg-jii)

- Cobra presença? Sim, relatório do meets

### Avaliações
- Provas:
  - Prova 1: 15/10
  - Prova 2: 10/12
  - Sub (possivelmente): 17/12
- Trabalhos: grupo de 3-5
  - Trabalho 1: 01/10
  - Trabalho 2: 24/11
- Exercícios: em grupo, feito em horário de aula (10-11 ao longo do semestre)

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SSC0526)
- Média de trabalhos (MT): média simples entre Trabalho 1 e 2
- Média 1 (M1): ponderada entre Prova 1 (40%) e Prova 2 (60%)
- Média 2 (M2): ponderada entre MT (70%) e Exercícios (8 melhores) (30%)
- **Média final**: ponderada entre M1 (60%) e M2 (40%) - **Os dois ≥ 5**, se não o menor deles

---
## SSC0577 - Seminários em Computação II
{: #SSC0577}

### Sobre
- Professora: Simone de Souza ([srocio@icmc.usp.br](mailto:srocio@icmc.usp.br))
- Plataforma: [E-Disciplinas](https://edisciplinas.usp.br/course/view.php?id=91734)
- Aula: [Google Meet](https://meet.google.com/igq-jcam-mhd)

- Cobra presença? [Sim, formulário](https://edisciplinas.usp.br/mod/url/view.php?id=3817317)
### Avaliações
- Resumo semanal do seminário: nota 0, 5, 7.5 ou 10.

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SSC0577)
- **Média dos resumos**: média das 70% melhores notas

---
## SCC0540 - Bases de Dados
{: #SCC0540 .bd}

### Sobre
- Professor: Jose Rodrigues Júnior ([junio@icmc.usp.br](mailto:junio@icmc.usp.br))
- Plataforma: [Tidia](https://ae4.tidia-ae.usp.br/portal/site/b82c61bd-1e05-4877-9366-f6ffc2babce7/)
- Aula: [Google Meet](https://meet.google.com/bhr-zuoa-zvz)

- Cobra presença? Sim, exercícios

### Avaliações
- Atividades quinzenais
- Projeto: 24/11

### Critérios
[Calculadora](/notes/bsi/calculadora-2021-2.html#SCC0540)
- Média de atividades (MA)
- Média do projeto (MP)
- **Média final**: Média simples entre MA e MP - **Os dois ≥ 5**, se não o menor deles

---
## SEP0584 - Contabilidade para Computação
{: #SEP0584 .acessorio}

### Sobre
- Professor: Humberto Bettini ([humberto@sc.usp.br](mailto:humberto@sc.usp.br)/[humberto.bettini.acessorio@gmail.com](mailto:humberto.bettini.acessorio@gmail.com))
- Plataforma: Não há, pra economizar dados do ciber espaço [_sic_]
- Aula: [Google Meet do Diógenes](https://meet.google.com/rku-xnuu-kjr) ([gravações](https://drive.google.com/folderview?id=1JckHfflmn7fx-PPAYQmuKDIpGg2KGEiB))

- Cobra presença? Sim, chamada em aulas aleatórias

### Avaliações
- Prova 1: 11/11, objetiva, aberta das 21h até 21h do dia seguinte
- Prova 2: a definir

### Critérios
- Média simples entre as duas provas (provavelmente)

---
