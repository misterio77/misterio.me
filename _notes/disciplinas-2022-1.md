---
title: "Disciplinas BSI 020 - 2022/1"
category: bsi
language: pt-br
noindex: true
date: 2021-11-29
draft: true
---

[Calculadora de notas](/notes/bsi/calculadora-2022-1.html)

## Horários
[Arquivo de calendário](https://misterio.me/files/2022/classes.ics)

- Segunda
  - 19:00 - 20:40: [Engenharia de Software](#SSC0527)
  - 21:00 - 22:40: [Modelagem da Produção](#SEP0301)
- Terça
  - 19:00 - 20:40: [Inteligência Artificial](#SCC0530)
  - 21:00 - 22:40: [Redes de Computadores](#SSC0540)
- Quarta
  - 19:00 - 20:40: [Laboratório de Bases de Dados](#SCC0541)
  - 21:00 - 22:40: [Laboratório de Bases de Dados](#SCC0541)
- Quinta
  - 19:00 - 20:40: [Modelagem da Produção](#SEP0301)
  - 21:00 - 22:40: [Engenharia de Software](#SSC0527)
- Sexta
  - 19:00 - 20:40: [Redes de Computadores](#SSC0540)
  - 21:00 - 22:40: [Inteligência Artificial](#SCC0530)

--- 
## SSC0527 - Engenharia de Software
{: #SSC0527 .engsoft}

### Sobre
- Professora: Elisa Nakagawa ([elisa@icmc.usp.br](mailto:elisa@icmc.usp.br))
- Plataforma: TBD
- Aula: TBD

- Cobra presença? TBD

### Avaliações
TBD

### Critérios
[Calculadora](/notes/bsi/calculadora-2022-1.html#SSC0527)
TBD

--- 
## SEP0301 - Modelagem da Produção
{: #SEP0301 .modprod}

### Sobre
- Professor: TBD
- Plataforma: TBD
- Aula: TBD

- Cobra presença? TBD

### Avaliações
TBD

### Critérios
[Calculadora](/notes/bsi/calculadora-2022-1.html#SEP0301)
TBD


--- 
## SCC0530 - Inteligência Artificial
{: #SCC0530 .ia}

### Sobre
- Professor: Alneu de Andrade Lopes ([alneu@icmc.usp.br](mailto:alneu@icmc.usp.br))
- Plataforma: TBD
- Aula: TBD

- Cobra presença? TBD

### Avaliações
TBD

### Critérios
[Calculadora](/notes/bsi/calculadora-2022-1.html#SCC0530)
TBD

--- 
## SSC0540 - Redes de Computadores
{: #SSC0540 .redes}

### Sobre
- Professor: Jó Ueyama ([joueyama@icmc.usp.br](mailto:joueyama@icmc.usp.br))
- Plataforma: TBD
- Aula: TBD

- Cobra presença? TBD

### Avaliações
TBD

### Critérios
[Calculadora](/notes/bsi/calculadora-2022-1.html#SSC0540)
TODO

---
## SCC0541 - Laboratório de Bases de Dados
{: #SCC0541 .lab .bd .labbd}

### Sobre
- Professor: Caetano Traina Junior ([caetano@icmc.usp.br](mailto:caetano@icmc.usp.br))
- Plataforma: TBD
- Aula: TBD

- Cobra presença? TBD

### Avaliações
TBD

### Critérios
[Calculadora](/notes/bsi/calculadora-2022-1.html#SCC0541)
TBD

---
